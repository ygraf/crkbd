# CRKBD configuration

CRKBD flash instructions:

Pull this project into `qmk_firmware/keyboards/crkbd/keymaps/yg`

Then execute the following comands in `qmk_firmware`:

Build: `qmk compile -kb crkbd -km yg`

Flash: `sudo make crkbd:yg:dfu`

# Troubleshoot

## Tap and hold problem

TODO
Usually I want:
```
#define IGNORE_MOD_TAP_INTERRUPT
```
in `config.h`.
Also:
```
#define TAPPING_TERM 200
```
in the same file should be checked.

Resources see [here](https://docs.qmk.fm/#/tap_hold)

## If everything is lost
Best starting point was `qmk_firmware/keyboards/crkbd/keymaps/default'
